<!DOCTYPE html>
<?php
  //do_action( 'genesis_doctype' );
  do_action( 'genesis_title' );
  do_action( 'genesis_meta' );
?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="google6d6b777de55e161b">
    <?php wp_head(); ?>
  </head>
  <?php
    genesis_markup( array(
      'html5'   => '<body %s>',
      'xhtml'   => sprintf( '<body class="%s">', implode( ' ', get_body_class() ) ),
      'context' => 'body',
    ) );
    do_action( 'genesis_before' );
  ?>

    <section id="top" class="top">
      <div class="wrapper">
        <a href="/" class="top__logo" title="Omni Translation home" itemprop="logo">Omni Translation</a>
        <a href="tel:0380247081" itemprop="telephone" class="btn btn-outline btn-outline--grey top__phone"><i class="top__phone--icon"></i>+603 8024 7081</a>
        <nav class="top__nav" role="navigation" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
          <?php wp_nav_menu( array(
            'theme_location' => 'top_nav',
            'container' => '',
            'items_wrap'      => '<ul>%3$s</ul>'
          )); ?>
        </nav>
        </div>
      </div>

    </section>