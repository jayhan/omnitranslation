  </div><!-- closing of role="main" div -->

  <footer class="footer" role="contentinfo" itemscope="itemscope" itemtype="http://schema.org/WPFooter">

    <div class="wrapper">

      <div class="footer__copyright">
        Copyright &copy; Omni-Translations <?php echo date('Y'); ?>. All rights reserved.
      </div>

    </div>


  </footer>
  <?php wp_footer(); //* we need this for plugins ?>

</body>
</html>
