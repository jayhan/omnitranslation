  <?php
    // Start the Loop.
    while ( have_posts() ) : the_post(); ?>

  <header class="header" role="banner">
    <div class="wrapper">
      <div class="header__title">
        <h1 class="page__title"><?php the_title(); ?></h1>
        <?php if(get_field('h2_subtitle')): ?>
          <h2 class="page__title--secondary"><?php the_field('h2_subtitle'); ?></h2>
        <?php endif; ?>
      </div>
    </div>
  </header>

  <div role="main" itemprop="mainContentOfPage" itemscope="itemscope" itemtype="http://schema.org/Blog" class="section--main">
    <div class="wrapper">
      <div class="content__main">
        <article id="post-<?php the_ID(); ?>" <?php post_class('article article--single'); ?>>
          <?php
            the_content();
            wp_link_pages( array(
              'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentyfourteen' ) . '</span>',
              'after'       => '</div>',
              'link_before' => '<span>',
              'link_after'  => '</span>',
            ) );

            edit_post_link( __( 'Edit', 'twentyfourteen' ), '<span class="edit-link">', '</span>' );
          ?>

          <?php genesis_do_author_box_single(); ?>
        </article>

        <?php comments_template() ?>

      </div>

      <?php endwhile; ?>