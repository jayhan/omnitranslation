<?php

  //* Start the engine
  require_once( get_template_directory() . '/lib/init.php' );

  //* Add HTML5 markup structure
  add_theme_support( 'html5' );

  // Register Styles
  function theme_styles() {
    wp_enqueue_style('source-sans-pro', '//fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700');
    wp_enqueue_style('normalize', get_stylesheet_directory_uri() . '/css/normalize.css');
    wp_enqueue_style('main', get_stylesheet_directory_uri() . '/css/main.css');
    wp_register_style('landing', get_stylesheet_directory_uri() . '/css/landing.css');
    wp_register_style('certified', get_stylesheet_directory_uri() . '/css/certified.css');
    wp_register_style('fancybox-css', get_stylesheet_directory_uri() . '/js/fancybox/jquery.fancybox.css');
    wp_register_style('tq-css', get_stylesheet_directory_uri() . '/css/tq.css');

    if(is_page_template( 'page-landing.php')) {
      wp_enqueue_style('landing');
    }

    if(is_page_template( 'page-certified-translation.php')) {
      wp_enqueue_style('certified');
      wp_enqueue_style('fancybox-css');
    }

    if(is_page_template( 'page-tq.php')) {
      wp_enqueue_style('tq-css');
    }
  }
  add_action('wp_enqueue_scripts', 'theme_styles');

  // Register Scripts
  function theme_js(){
    wp_enqueue_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js', '', '', true);
    wp_enqueue_script('modernizr', get_stylesheet_directory_uri() . '/js/vendor/modernizr-2.8.0.min.js');
    wp_enqueue_script('scripts', get_stylesheet_directory_uri() . '/js/scripts.js', array('jquery'), '', true);
    wp_register_script('cycle', get_stylesheet_directory_uri() . '/js/vendor/jquery.cycle.min.js', array('jquery'), '', true);
    wp_register_script('lettering', get_stylesheet_directory_uri() . '/js/lettering.min.js', array('jquery'), '', true);
    wp_register_script('fancybox-js', get_stylesheet_directory_uri() . '/js/fancybox/jquery.fancybox.pack.js', array('jquery'), '', true);
    wp_register_script('fancybox-media', get_stylesheet_directory_uri() . '/js/fancybox/helpers/jquery.fancybox-media.js', array('fancybox-js'), '', true);

    if(is_front_page()){
      wp_enqueue_script('cycle');
    }
    if(is_page_template('page-certified-translation.php')) {
      wp_enqueue_script('lettering');
      wp_enqueue_script('fancybox-js');
      wp_enqueue_script('fancybox-media');
    }
  }
  add_action('wp_enqueue_scripts', 'theme_js');

  function enable_slick() {
  if (is_page_template('page-certified-translation.php')){
    echo
      '<script>
        jQuery(document).ready(function() {
          jQuery(".fancybox-media").fancybox({
            openEffect  : "none",
            closeEffect : "none",
            padding : 0,
            helpers : {
              media : {}
            }
          });
        });
      </script>';
  }
};
add_action('wp_footer', 'enable_slick', 100);

  //* Unregister layout settings
  genesis_unregister_layout( 'content-sidebar-sidebar' );
  genesis_unregister_layout( 'sidebar-content-sidebar' );
  genesis_unregister_layout( 'sidebar-sidebar-content' );

  //* Unregister secondary sidebar
  unregister_sidebar( 'sidebar-alt' );
  unregister_sidebar( 'header-right' );

  // Register Footer Sidebars
  add_action( 'widgets_init', 'omni_register_sidebars' );

  function omni_register_sidebars() {
    register_sidebar(array(
      'name' => __( 'Footer One' ),
      'id' => 'footer-one',
      'description' => __( 'Footer One.' ),
      'before_widget' => '<div class="footer__block %2$s">',
      'after_widget'  => '</div>',
      'before_title'  => '<h5>',
      'after_title'   => '</h5>'
    ));
    register_sidebar(array(
      'name' => __( 'Footer Two' ),
      'id' => 'footer-two',
      'description' => __( 'Footer Two.' ),
      'before_widget' => '<div class="footer__block %2$s">',
      'after_widget'  => '</div>',
      'before_title'  => '<h5>',
      'after_title'   => '</h5>'
    ));
    register_sidebar(array(
      'name' => __( 'Footer Three' ),
      'id' => 'footer-three',
      'description' => __( 'Footer Three.' ),
      'before_widget' => '<div class="footer__block %2$s">',
      'after_widget'  => '</div>',
      'before_title'  => '<h5>',
      'after_title'   => '</h5>'
    ));
    register_sidebar(array(
      'name' => __( 'Footer Four' ),
      'id' => 'footer-four',
      'description' => __( 'Footer Four.' ),
      'before_widget' => '<div class="footer__block %2$s">',
      'after_widget'  => '</div>',
      'before_title'  => '<h5>',
      'after_title'   => '</h5>'
    ));
    register_sidebar(array(
      'name' => __( 'Footer Social' ),
      'id' => 'footer-social',
      'description' => __( 'Footer Social.' ),
      'before_widget' => '<div class="%2$s">',
      'after_widget'  => '</div>',
      'before_title'  => '<h5>',
      'after_title'   => '</h5>'
    ));
    register_sidebar(array(
      'name' => __( 'Right Sidebar' ),
      'id' => 'right-sidebar',
      'description' => __( 'Widget area for right sidebar' ),
      'before_widget' => '<div class="sidebar %2$s">',
      'after_widget'  => '</div>',
      'before_title'  => '<h5>',
      'after_title'   => '</h5>'
    ));
    register_sidebar(array(
      'name' => __( 'Quote Sidebar' ),
      'id' => 'quote-sidebar',
      'description' => __( 'Widget area for quote sidebar' ),
      'before_widget' => '<div class="sidebar %2$s">',
      'after_widget'  => '</div>',
      'before_title'  => '<h5>',
      'after_title'   => '</h5>'
    ));
    register_sidebar(array(
      'name' => __( 'Article Sidebar' ),
      'id' => 'article-sidebar',
      'description' => __( 'Widget area for article sidebar' ),
      'before_widget' => '<div class="sidebar %2$s">',
      'after_widget'  => '</div>',
      'before_title'  => '<h5>',
      'after_title'   => '</h5>'
    ));
  }

  //Register Menu Bars
  register_nav_menus( array(
    'top_nav' => 'Main Navigation at Top',
    'footer_main' => 'Main Footer Menu',
    'footer_second' => 'Secondary Footer Menu'
  ) );

  add_shortcode('wp_caption', 'fixed_img_caption_shortcode');
  add_shortcode('caption', 'fixed_img_caption_shortcode');
  function fixed_img_caption_shortcode($attr, $content = null) {
      if ( ! isset( $attr['caption'] ) ) {
          if ( preg_match( '#((?:<a [^>]+>\s*)?<img [^>]+>(?:\s*</a>)?)(.*)#is', $content, $matches ) ) {
              $content = $matches[1];
              $attr['caption'] = trim( $matches[2] );
          }
      }
      $output = apply_filters('img_caption_shortcode', '', $attr, $content);
      if ( $output != '' )
          return $output;
      extract(shortcode_atts(array(
          'id'    => '',
          'align' => 'alignnone',
          'width' => '',
          'caption' => ''
      ), $attr));
      if ( 1 > (int) $width || empty($caption) )
          return $content;
      if ( $id ) $id = 'id="' . esc_attr($id) . '" ';
      return '<div ' . $id . 'class="wp-caption ' . esc_attr($align) . '" style="width: ' . $width . 'px">'
      . do_shortcode( $content ) . '<p class="wp-caption-text">' . $caption . '</p></div>';
  }

  //add h2_subtitle to post articles
  add_filter('genesis_entry_header', 'add_subtitle');

  function add_subtitle(){
    if(get_field('h2_subtitle')) {
      echo '<h2 class="page__title--secondary">' . get_field("h2_subtitle") . '</h2>';
    }
  }

  // modify breadcrumb's separator
  add_filter( 'genesis_breadcrumb_args', 'modify_breadcrumb' );

  function modify_breadcrumb($args){
    $args['labels']['prefix'] = '';
    $args['sep'] = ' &raquo; ';
    return $args;
  }

  //* Modify comments title text in comments
  add_filter( 'genesis_title_comments', 'modify_title_comments' );
  function modify_title_comments() {
    $commentNum = get_comments_number();
    if($commentNum <= 1) {
      $comment = ' comment';
    } else {
      $comment = ' comments';
    }
    $title = '<h3>' . $commentNum . ' ' . $comment . '</h3>';
    return $title;
  }


  // add meta tag
  add_action('genesis_meta', 'omni_robot_meta');
  function omni_robot_meta(){
    echo '<meta name="ROBOT" content="index, follow">' .
    '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
  }




?>