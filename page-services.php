<?php
/*
Template Name: Services
*/
?>

<?php get_header(); ?>

  <?php
    // Start the Loop.
    while ( have_posts() ) : the_post(); ?>

  <header class="header header--services" role="banner">
    <div class="wrapper">
      <div class="header__title">
        <h1 class="page__title"><?php the_title(); ?></h1>
      </div>
      <?php if(get_field('h2_subtitle')): ?>
        <h2 class="page__title--secondary">
          <?php the_field('h2_subtitle'); ?>
        </h2>
      <?php endif; ?>

      <?php if(get_field('orange_box_header')): ?>
      <div class="header__sub">
        <?php the_field('orange_box_header'); ?>
      </div>
      <?php endif; ?>

    </div>
  </header>

  <div role="main" itemprop="mainContentOfPage">

    <section id="leader" class="section section--larger a-center">
      <div class="wrapper">

        <?php the_field('first_block'); ?>

      </div>
    </section>

    <section id="experts" class="section section--grey a-center">
      <div class="wrapper">
        <?php the_field('second_block'); ?>
      </div>
    </section>

    <section id="notarisation" class="section section--larger a-center">
      <div class="wrapper">
        <?php the_field('third_block'); ?>
      </div>
    </section>

    <section id="delivery-typesetting" class="section section--larger section--blue a-center">
      <div class="wrapper">
        <div class="col-8">
          <div class="row">
            <?php the_field('fourth_block'); ?>
          </div>
        </div>
      </div>
    </section>

    <?php endwhile; ?>

<?php get_template_part('cta'); ?>

<?php get_footer(); ?>