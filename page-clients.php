<?php
/*
Template Name: Clients
*/
?>

<?php get_header() ?>

  <?php
    // Start the Loop.
    while ( have_posts() ) : the_post(); ?>

  <header class="header header--clients" role="banner">
    <div class="wrapper">
      <div class="header__title">
        <h1 class="page__title"><?php the_title(); ?></h1>
        <?php if(get_field('h2_subtitle')): ?>
          <h2 class="page__title--secondary">
            <?php the_field('h2_subtitle'); ?>
          </h2>
        <?php endif; ?>
      </div>

      <?php if(get_field('orange_box_header')): ?>
      <div class="header__sub">
        <?php the_field('orange_box_header'); ?>
      </div>
      <?php endif; ?>


    </div>
  </header>

  <div role="main" itemprop="mainContentOfPage">

    <section id="industries" class="section a-center">
      <div class="wrapper">
        <?php the_field('industries_list'); ?>
      </div>
    </section>

    <section id="clients" class="section a-center">
      <div class="wrapper">
        <?php the_field('clients_logo'); ?>
      </div>
    </section>

    <?php endwhile; ?>

<?php get_template_part('cta'); ?>

<?php get_footer() ?>