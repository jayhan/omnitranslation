<?php
/*
Template Name: Certified Translation
*/
?>

<?php get_header() ?>

  <?php
    // Start the Loop.
    while ( have_posts() ) : the_post(); ?>

  <header class="header header--certified" role="banner">
    <div class="wrapper">
      <div class="row">
        <div class="fl-9">
          <div class="header__title">
            <h1 class="page__title"><?php the_title(); ?></h1>
            <p class="page__description"><?php the_content(); ?></p>
            <div class="header__cta"></div><a href="/get-a-fast-and-free-translation-quote-now/" class="btn btn-md">Get a FREE Quote Now</a><a href="<?php the_field('youtube_video_link'); ?>" class="btn btn-md btn-outline btn-playvideo fancybox-media"><i></i>Play Video</a></div>
          </div>
        </div>
      </div>
    </div>
  </header>

  <div role="main" itemprop="mainContentOfPage">

    <!-- 1st section -->
    <section id="certified-guarantee" class="section section__certified--guarantee">
      <div class="wrapper">
        <div class="row">
          <div class="fl-6">
            <?php the_field('section_1'); ?>
          </div>
        </div>

      </div>
    </section>

    <!-- 2nd section -->
    <section id="certified-documents" class="section section--white section__certified--documents">
      <div class="wrapper">
        <div class="row">
          <div class="fl-6">
            <img src="<?php echo get_stylesheet_directory_uri() . '/img/certified-documents.png' ?>" alt="Certified Translation of Birth Certificates, Driving License, Passports etc" class="image__certified--documents">
          </div>
          <div class="fl-6">
            <?php the_field('section_2'); ?>
          </div>
        </div>
      </div>
    </section>

    <!-- 3rd section -->
    <section id="certified-order" class="section section--darkgrey section__certified--order">
      <div class="wrapper">
        <h3 class="section__title a-center">
          <?php the_field('order_header'); ?>
        </h3>
        <?php the_field('order_steps'); ?>
        <div class="row order-questions">
          <div class="fl-6">
            <?php the_field('order_question_1'); ?>
          </div>
          <div class="fl-6">
            <?php the_field('order_question_2'); ?>
          </div>
        </div>
      </div>
    </section>

    <!-- 4th section -->
    <section id="certified-getstarted" class="section section__certified--cta">
      <div class="wrapper">
        <div class="row">
          <div class="fl-8">
            <?php the_field('cta'); ?>
          </div>
          <img src="<?php echo get_stylesheet_directory_uri() . '/img/certified-cta-hero.png' ?>" alt="Get Started Today" class="cta__hero">
        </div>

      </div>
    </section>

    <!-- 5th section - clients -->
    <section id="bottom-cta">
      <div class="cta__clients">
        <div class="wrapper">
          <div class="cta__clients--omni">
            OmniTranslation
          </div><!--
          --><div class="cta__clients--client">
            <span>Our satisfied, repeat clients</span>
            <div>
              Starbucks, KFC, Maxis, Celcom, Adidas, Ambank, Zurich, Hilton, Digi, Levi's
            </div>
          </div>
        </div>
      </div>
    </section>


    <?php endwhile; ?>

<?php get_footer() ?>