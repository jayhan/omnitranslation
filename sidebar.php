  <div class="content__sidebar" role="complementary" itemscope="itemscope" itemtype="http://schema.org/WPSideBar">

    <?php if (is_page(33)){
      dynamic_sidebar('quote-sidebar');
    } elseif (is_home() || is_single()){
    	dynamic_sidebar('article-sidebar');
    } else {
      dynamic_sidebar('right-sidebar');
    }?>
  </div>
