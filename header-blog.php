<header class="header header--blue header--post" role="banner">
  <div class="wrapper">
    <div class="header__title">
      <h4 class="page__title"><a href="/articles">Articles</a></h4>
    </div>
  </div>
</header>