<?php
/*
Template Name: Languages
*/
?>

<?php get_header(); ?>

  <?php
    // Start the Loop.
    while ( have_posts() ) : the_post(); ?>

  <header class="header header--languages" role="banner">
    <div class="wrapper">
      <div class="header__title">
        <h1 class="page__title"><?php the_title(); ?></h1>
        <?php if(get_field('h2_subtitle')): ?>
          <h2 class="page__title--secondary">
            <?php the_field('h2_subtitle'); ?>
          </h2>
        <?php endif; ?>
      </div>

      <?php if(get_field('orange_box_header')): ?>
      <div class="header__sub">
        <?php the_field('orange_box_header'); ?>
      </div>
      <?php endif; ?>

    </div>
  </header>

  <div role="main" itemprop="mainContentOfPage">

    <section id="languages-list" class="section a-center">
      <div class="wrapper">

        <?php the_field('languages_we_translate'); ?>

      </div>
    </section>

    <section id="language-pairs" class="section section--grey a-center">
      <div class="wrapper">

        <?php the_field('language_pairs'); ?>

      </div>
    </section>

    <?php endwhile; ?>

<?php get_template_part('cta'); ?>

<?php get_footer(); ?>