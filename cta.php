  <section id="bottom-cta">
    <div class="cta__main">
      <div class="wrapper">
        <div class="cta__main--left">
          <h3 class="cta__title">Quality Translations. Unbeatable Prices</h3>
          <p>We're - experienced, quality conscious, deliver on time...<br>and best of all <strong>affordable</strong>.</p>
        </div>
        <div class="cta__main--right">
          <a class="btn btn-lg" href="<?php echo get_permalink('33'); ?>">Get a FREE translation quote now!</a>
        </div>
      </div>
    </div><!--
    --><div class="cta__clients">
      <div class="wrapper">
        <div class="cta__clients--omni">
          OmniTranslation
        </div><!--
        --><div class="cta__clients--client">
          <span>Our satisfied, repeat clients</span>
          <div>
            Starbucks, KFC, Maxis, Celcom, Adidas, Ambank, Zurich, Hilton, Digi, Levi's
          </div>
        </div>

      </div>
    </div>
  </section>