<?php
/*
Template Name: Thank You Page
*/
?>

<!DOCTYPE html>
<?php
  //do_action( 'genesis_doctype' );
  //do_action( 'genesis_title' );
  do_action( 'genesis_meta' );
?>
    <title>Thanks for enquiring with us! - Omni-Translation</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="google6d6b777de55e161b">
    <meta name="robots" content="noindex, nofollow">
    <?php wp_head(); ?>
  </head>
  <?php
    genesis_markup( array(
      'html5'   => '<body %s>',
      'xhtml'   => sprintf( '<body class="%s">', implode( ' ', get_body_class() ) ),
      'context' => 'body',
    ) );
    do_action( 'genesis_before' );
  ?>

  <header role="banner" class="top--tq">
    <a href="/" class="top__logo" title="Omni Translation home" itemprop="logo">Omni Translation</a>
  </header>


  <div role="main" itemprop="mainContentOfPage">

      <section class="tq">
        <header class="tq__header">
          <h1><i></i> Thank You!</h1>
        </header>
        <div class="tq__body">
          <p>We will respond to you within 24 hrs during normal business days. Our office hours are 9am - 6pm. Mondays to Fridays.</p>
          <div class="discount">
            <p><strong>Get Discounts and Offers!  Like us on Facebook to be notified.</strong></p>
            <div class="discount__btn">
              <a href="https://www.facebook.com/omni.translation" class="btn btn-lg btn--facebook">
                <svg class="svg__facebook" width="10" height="18" viewBox="0 0 10 18" xmlns="http://www.w3.org/2000/svg"><title>tq-facebook</title><g fill="#fff"><path d="M9.99 0H7.137c-2.38 0-4.122 1.982-4.122 4.52v1.746H0v2.93h3.015V18h3.408V9.197H9.99v-2.93H6.423V4.202c0-.632.398-1.11.714-1.11H9.99V0z"/></g></svg> Like us on Facebook!
              </a>
              <svg class="svg__arrow" xmlns="http://www.w3.org/2000/svg" width="47" height="47" viewBox="0 0 47 47"><path fill="#F79520" d="M.347 37.154l.178.145c-.452.052-.755.723-.3 1.112l.308.26c-.044.4.358.787.786.67 1.698 1.45 3.4 2.896 5.105 4.34l1.705 1.56c.09.082.19.12.29.127l1.756 1.487c.38.32.785.05.892-.323.07.037.14.075.21.11.048.026.094.053.143.077.225.11.416.067.554-.05l.04.03c.65.48 1.34-.51.725-1.01-1.27-1.037-2.536-2.074-3.804-3.11-.09-.197-.14-.23-.473-.577-.105-.11-.234-.128-.344-.092-.775-.632-1.55-1.266-2.324-1.9 8.715 1.05 17.57-2.295 24.13-8.11 1.704-1.51 3.237-3.164 4.616-4.932 7.17-6.42 12.908-15.76 11.856-25.503-.036-.336-.617-.318-.6.028.073 1.646-.028 3.272-.28 4.87.07-1.96-.19-3.978-.877-6.047-.16-.487-.777-.357-.98.006-.288-.462-1.11-.268-1.003.366.214 1.267.297 2.54.266 3.814-.97 4.935-2.468 9.838-4.674 14.36-.547.904-1.127 1.788-1.733 2.647-1.674 1.943-3.557 3.716-5.525 5.28-3.985 3.168-8.41 6.058-13.07 8.128-.723.32-1.454.605-2.19.865-2.416.615-4.884 1.012-7.374 1.192 1.777-.866 3.482-1.857 5.14-2.933.354-.23.274-.688.012-.942.365-.434-.238-1.118-.796-.92-.588.21-1.177.422-1.763.638-.117-.29-.43-.498-.808-.33-.61.27-1.205.568-1.793.876C5.86 34.514 3.284 35.41.6 36.04c-.534.126-.67.772-.253 1.114zm42.64-24.33c1.076-2.975 1.57-6.125 1.258-9.363l.03-.17c.555 3.332-.03 6.52-1.287 9.534zM2.248 37.11l-.195-.16c.236-.063.47-.132.704-.198-.172.116-.34.237-.51.357zM30.323 28.7c-.39.37-.785.736-1.19 1.09-.855.574-1.732 1.112-2.624 1.623 1.307-.862 2.58-1.77 3.814-2.713zm-6.978 6.495c-5.164 2.89-11.093 4.385-16.97 3.776.276-.037.553-.076.83-.116 5.63.447 11.19-.974 16.14-3.66zm-7.48 1.698c1.023-.258 2.037-.552 3.04-.883-1.32.502-2.673.907-4.047 1.21.336-.105.67-.213 1.005-.327z"/></svg>
            </div>
          </div>
        </div>
        <footer class="tq__footer">
          <p>Hundreds of businesses trust us with their translation. We Deliver Exceptional quality at Unbeatable Prices. Trust Us …They Do!</p>
          <div class="cta__clients--client">
            <div>
              Starbucks, KFC, Maxis, Celcom, Adidas, Ambank, Zurich, Hilton, Digi, Levi's
            </div>
          </div>
        </footer>
      </div>

  </div>

  <footer class="footer--tq" role="contentinfo" itemscope="itemscope" itemtype="http://schema.org/WPFooter">
    Copyright &copy; Omni-Translations 2015. All rights reserved.
  </footer>

  <?php wp_footer(); //* we need this for plugins ?>

  </body>
</html>

