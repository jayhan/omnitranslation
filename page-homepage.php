<?php
/*
Template Name: Home Page
*/
?>

<?php get_header(); ?>

  <?php
    // Start the Loop.
    while ( have_posts() ) : the_post(); ?>

  <header class="header header--home" role="banner">
    <div class="wrapper">
      <div class="header__title a-center">
        <h1 class="page__title" itemprop="serviceType" itemtype="http://schema.org/Service"><?php the_title(); ?></h1>
        <h2 class="page__title--secondary"><?php the_field('h2_subtitle'); ?></h2>
      </div>
      <div class="header__sub header__sub--home">
        <div class="header__sub--left">
          <?php the_field('blue_box_header'); ?>
        </div><div class="header__sub--right">
          <a href="<?php echo get_permalink('33'); ?>" class="btn btn--ctahome">Get a <span>Free</span> Translation Quote!</a>
          <em><?php the_field('text_below_cta_button'); ?></em>
          <div class="header__humancta">
          </div>
        </div>
      </div>
    </div>
  </header>

  <div role="main" itemprop="mainContentOfPage">

    <section id="trust" class="section section--larger a-center">
      <div class="wrapper">
        <?php the_field('trusted_by_companies'); ?>
      </div>
    </section>

    <section id="why-omni"  class="section section--grey a-center">
      <div class="wrapper">
        <h3 class="section__title"><?php the_field('why_omni-translation'); ?></h3>
        <div class="why-wrapper">
          <div id="pager" class="why-pager">
            <?php the_field('slider_nav'); ?>
          </div>
          <div class="cycle-slideshow why-slider" data-cycle-pager="#pager"
      data-cycle-pager-template="" data-cycle-timeout="8000" data-cycle-slides="> div" data-cycle-auto-height="container" >
            <div class="why">
              <img src="<?php echo get_stylesheet_directory_uri() . '/img/home-slider-1.jpg' ?>" alt="Malaysia’s PREMIER Translation Agency" class="why__image">
              <div class="why__content">
                <?php the_field('slider_content_1'); ?>
              </div>
            </div>
            <div class="why">
              <img src="<?php echo get_stylesheet_directory_uri() . '/img/home-slider-2.jpg' ?>" alt="Our Experience = Your Peace of Mind" class="why__image">
              <div class="why__content">
                <?php the_field('slider_content_2'); ?>
              </div>
            </div>
            <div class="why">
              <img src="<?php echo get_stylesheet_directory_uri() . '/img/home-slider-3.jpg' ?>" alt="Translate any subject, any size" class="why__image">
              <div class="why__content">
                <?php the_field('slider_content_3'); ?>
              </div>
            </div>
            <div class="why">
              <img src="<?php echo get_stylesheet_directory_uri() . '/img/home-slider-4.jpg' ?>" alt="30 languages | 150+ native translators" class="why__image">
              <div class="why__content">
                <?php the_field('slider_content_4'); ?>
              </div>
            </div>
            <div class="why">
              <img src="<?php echo get_stylesheet_directory_uri() . '/img/home-slider-5.jpg' ?>" alt="The Omni promise: high quality + best price + on-time delivery" class="why__image">
              <div class="why__content">
                <?php the_field('slider_content_5'); ?>
              </div>
            </div>

          </div>
        </div>
      </div>
    </section>

  <?php endwhile; ?>

<?php get_template_part('cta'); ?>

<?php get_footer(); ?>