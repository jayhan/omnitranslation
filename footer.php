  </div><!-- closing of role="main" div -->

  <footer class="footer" role="contentinfo" itemscope="itemscope" itemtype="http://schema.org/WPFooter">

    <div class="wrapper">
      <div class="row">

        <?php dynamic_sidebar( 'footer-one' ); ?><?php dynamic_sidebar( 'footer-two' ); ?><?php dynamic_sidebar( 'footer-three' ); ?><?php dynamic_sidebar( 'footer-four' ); ?>

      </div>

      <div class="footer__nav">
        <nav class="footer__nav--main" role="navigation" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
          <?php wp_nav_menu( array(
            'theme_location' => 'footer_main',
            'container' => '',
            'items_wrap'      => '<ul>%3$s</ul>'
          )); ?>
        </nav>
        <nav class="footer__nav--second" role="navigation" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
          <?php wp_nav_menu( array(
            'theme_location' => 'footer_second',
            'container' => '',
            'items_wrap'      => '<ul>%3$s</ul>'
          )); ?>
        </nav>
      </div>

      <div class="footer__social-wrapper">
        <?php dynamic_sidebar( 'footer-social' ); ?>
      </div>

      <div class="footer__copyright">
        Copyright &copy; Omni-Translations <?php echo date('Y'); ?>. All rights reserved.
      </div>

    </div>


  </footer>
  <?php wp_footer(); //* we need this for plugins ?>

</body>
</html>
