<?php
/*
Template Name: Contact Us
*/
?>

<?php get_header() ?>

  <?php
    // Start the Loop.
    while ( have_posts() ) : the_post(); ?>

  <header class="header header--contact" role="banner">
    <div class="wrapper">
      <div class="header__title">
        <h1 class="page__title"><?php the_title(); ?></h1>
        <?php if(get_field('h2_subtitle')): ?>
          <h2 class="page__title--secondary">
            <?php the_field('h2_subtitle'); ?>
          </h2>
        <?php endif; ?>
      </div>
      <div class="contact-details" itemscope itemtype="http://schema.org/Organization">
        <div>
          <?php the_field('call'); ?>
        </div>
        <div>
          <?php the_field('fax'); ?>
        </div>
        <div>
          <?php the_field('gps'); ?>
        </div>
      </div>
    </div>
  </header>

  <div role="main" itemprop="mainContentOfPage" class="section--main">
    <div class="wrapper">
      <div class="content__main">
        <article id="post-<?php the_ID(); ?>" <?php post_class('article article--page'); ?>>
          <?php
            the_content();
            wp_link_pages( array(
              'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentyfourteen' ) . '</span>',
              'after'       => '</div>',
              'link_before' => '<span>',
              'link_after'  => '</span>',
            ) );

            edit_post_link( __( 'Edit', 'twentyfourteen' ), '<span class="edit-link">', '</span>' );
          ?>
        </article>
        <?php endwhile; ?>
      </div>
      <?php get_sidebar(); ?>
    </div>

<?php get_footer() ?>