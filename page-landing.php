<?php
/*
Template Name: Language Landing Page
*/
?>

<?php get_template_part('lp-header'); ?>

  <?php
    // Start the Loop.
    while ( have_posts() ) : the_post(); ?>

  <header id="free-quote" class="header header--landing" role="banner">
    <div class="wrapper">
      <div class="header__title">
        <h1 class="page__title" itemprop="serviceType" itemtype="http://schema.org/Service"><?php the_title(); ?>
          <?php if(get_field('h2_subtitle')): ?>
            <span><?php the_field('h2_subtitle'); ?></span>
          <?php endif; ?>
        </h1>
      </div>

      <div class="row">
        <div class="fl-8">
          <?php the_field('header'); ?>
        </div>
        <div id="form" class="header__landingform">
          <div class="landingform__header">
            <?php the_field('form_header'); ?>
          </div>
          <div class="landingform__body">
            <?php the_content(); ?>
          </div>
        </div>
      </div>

      <div class="header__sub">
        <?php the_field('header_sub'); ?>
      </div>




    </div>
  </header>
  <div role="main" itemprop="mainContentOfPage">

    <!-- logos -->
    <div class="section section--logos">
      <div class="wrapper">
        <div class="cta__clients--client">
          <span><?php the_field('client_list'); ?></span>
          <div>
            Starbucks, KFC, Maxis, Celcom, Adidas, Ambank, Zurich, Hilton, Digi, Levi's
          </div>
        </div>
      </div>
    </div>

    <!-- Landing Page with video -->
    <?php if(get_field('language_video')): ?>
    <section class="section section--larger section--video">
      <div class="wrapper">
        <h3 class="section__title section__title--light">
          Watch this video to find out more about our translation service
        </h3>
        <div class="col-8">
          <div class="video-wrapper">
            <iframe width="640" height="360" src="//www.youtube.com/embed/<?php the_field('language_video'); ?>?rel=0" frameborder="0" allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </section>
    <?php endif; ?>

    <!-- 1st section -->
    <div class="section section--larger">
      <div class="wrapper">
        <div class="row">
          <?php the_field('section_1'); ?>
        </div>
      </div>
    </div>

    <!-- Second Section -->
    <div class="section section--grey section--doyouknow">
      <div class="wrapper">
        <div class="row">
          <div class="fl-10">
            <?php the_field('section_2'); ?>
          </div>
          <div class="fl-2">
            <div class="human">
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Third Section -->
    <div class="section section--larger">
      <div class="wrapper">
        <?php the_field('section_3'); ?>
      </div>
    </div>

    <!-- Fourth Section -->
    <div class="section section--blue section--larger">
      <div class="wrapper">
        <div class="row">
          <div class="fl-8">
            <?php the_field('section_4'); ?>
          </div>
          <div class="fl-4 a-center">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/services-shield.png" alt="Quality Guaranteed, Certified Translation from Omni-Translation">
          </div>
        </div>

      </div>
    </div>

    <!-- Fifth Section -->
    <div class="section section--larger section--last">
      <div class="wrapper">
        <div class="row">
          <div class="fl-8">
            <?php the_field('section_last'); ?>
          </div>
          <div class="fl-4 a-center">
            <img src="/wp-content/uploads/2014/10/footer-badge.png" alt="Our Triple Guarantee: Accuracy, Best Price, Native Translators" width="225">
          </div>
        </div>

      </div>
    </div>

  <?php endwhile; ?>

<?php get_template_part('lp-footer'); ?>














