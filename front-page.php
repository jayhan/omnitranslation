<?php get_header(); ?>

  <header class="header header--home" role="banner">
    <div class="wrapper">
      <div class="header__title a-center">
        <h1 class="page__title">Professional Certified Translation Services</h1>
        <h2 class="page__title--secondary">Translate from English to Malay, Chinese, Tamil, Spanish, Japanese, Arabic, Indonesian, Korean. Certified Translation for 30 Languages</h2>
      </div>
      <div class="header__sub header__sub--home">
        <div class="header__sub--left">
          <h3>With Omni-Translation, you get:</h3>
          <ul class="li--morespace">
            <li><strong>Experience:</strong> We've translated over 11,000 projects into 30 languages.</li>
            <li><strong>Quality:</strong> Only Certified Native Translators.</li>
            <li><strong>Guarantee:</strong> High Quality with a Price Match Guarantee</li>
          </ul>
        </div><div class="header__sub--right">
          <a href="/get-a-fast-and-free-translation-quote-now/" class="btn btn--ctahome">Get a <span>Free</span> Translation Quote!</a>
          <em>From as Low as RM60</em>
          <div class="header__humancta">

          </div>
        </div>
      </div>
    </div>
  </header>

  <div role="main" itemprop="mainContentOfPage">

    <section id="trust" class="section section--larger a-center">
      <div class="wrapper">
        <h3 class="section__title">
          Companies that trust our translation include…
        </h3>
        <p class="col-8">We earn trust by delivering accurate and affordable translations. Our high client retention rate and growth is proof of client satisfaction. Trust us. They Do!</p>
        <div class="clients-logo__row1">
          <span class="clients-logo--a">Starbucks, KFC, Maxis, Celcom, Adidas,</span>
          <span class="clients-logo--b">Ambank, Zurich, Hilton, Digi, Levi's</span>
        </div>
      </div>
    </section>

    <section id="why-omni"  class="section section--grey a-center">
      <div class="wrapper">
        <h3 class="section__title">Why Omni-Translation?</h3>
        <div class="why-wrapper">
          <div id="pager" class="why-pager">
            <a href="#"><i>1</i><span>Malaysia’s Premier Translation Agency</span></a>
            <a href="#"><i>2</i><span>Our experience = Your peace of mind</span></a>
            <a href="#"><i>3</i><span>Translate any subject, any size</span></a>
            <a href="#"><i>4</i><span>30 languages, 150+ native translators</span></a>
            <a href="#"><i>5</i><span>High quality, best price, on-time delivery</span></a>
          </div>
          <div class="cycle-slideshow why-slider" data-cycle-pager="#pager"
      data-cycle-pager-template="" data-cycle-timeout="8000" data-cycle-slides="> div" data-cycle-auto-height="container" >
            <div class="why">
              <img src="<?php echo get_stylesheet_directory_uri() . '/img/home-slider-1.jpg' ?>" alt="Malaysia’s PREMIER Translation Agency" class="why__image">
              <div class="why__content">
                <h4>Malaysia’s PREMIER Translation Agency</h4>
                <p>With over 11,000 translation projects to our credit and with over 900 businesses using our services regularly you can bet your project is in safe hands. We translate for Government Agencies (eg Pemandu, Ministry of Finance), MNCs, Banks, Legal Firms, SMEs and individuals. </p>
              </div>
            </div>
            <div class="why">
              <img src="<?php echo get_stylesheet_directory_uri() . '/img/home-slider-2.jpg' ?>" alt="Our Experience = Your Peace of Mind" class="why__image">
              <div class="why__content">
                <h4>Our Experience = Your Peace of Mind</h4>
                <p>Established in 2002, Omni-Translation is a multi-lingual translation agency in Malaysia. We translate from English to Malay, Chinese, Japanese, Arabic 30 major languages. Our sustained growth and success can be attributed to our commitment to quality, low prices and excellent project management. When every word matters, you want the professionals. Trust Omni-Translation to get the job done right!</p>
                <p><a href="#" class="btn btn-outline">View our full list of services</a></p>
              </div>
            </div>
            <div class="why">
              <img src="<?php echo get_stylesheet_directory_uri() . '/img/home-slider-3.jpg' ?>" alt="Translate any subject, any size" class="why__image">
              <div class="why__content">
                <h4>Translate any subject, any size</h4>
                <p>We translate 100 word birth certificates from Malay to English or 50,000 word technical manuals from Chinese to English. Our Certified Translations are accepted by the American, Canadian, British and Australian Embassies in Malaysia. Translate your Visa and migration documents Certified by Omni-Translation.  We translate Malaysian Birth Certificates and Marriage Certificates from Malay to English for only RM70.</p>
              </div>
            </div>
            <div class="why">
              <img src="<?php echo get_stylesheet_directory_uri() . '/img/home-slider-4.jpg' ?>" alt="30 languages | 150+ native translators" class="why__image">
              <div class="why__content">
                <h4>30 languages | 150+ native translators</h4>
                <p>Our 150+ translators have a minimum of 5 years’ experience and many are certified by translation bodies. They only work in their native / mother tongue. Our translators fully understand the need to be 100% accurate and in-context, and to deliver the work on time. We offer translation for more than 30 languages.</p>
                <p><a href="#" class="btn btn-outline">View the languages we translate</a></p>
              </div>
            </div>
            <div class="why">
              <img src="<?php echo get_stylesheet_directory_uri() . '/img/home-slider-5.jpg' ?>" alt="The Omni promise: high quality + best price + on-time delivery" class="why__image">
              <div class="why__content">
                <h4>The Omni promise:<br> high quality + best price + on-time delivery</h4>
                <p>We take great pride in providing our clients with high quality translations at unbeatable prices.</p>
                <ul class="omni-quality__list">
                  <li>We will match the quote from any reputable translation agency in Malaysia.</li>
                  <li>We will rework the translation if you are not 100% satisfied.</li>
                  <li>We will give you a 10% discount if we do not deliver on time.</li>
                  <li>We will not divulge the contents of you projects to anyone or contact your clients directly.</li>
                </ul>
              </div>
            </div>

          </div>
        </div>
      </div>
    </section>

<?php get_template_part('cta'); ?>

<?php get_footer(); ?>